# The Siege of Remontel

A Star Trek fan production.

## Story

The Siege of Remontel is a text-based adventure set in the world of Star Trek.

You play an ensign serving on the lower decks of the USS Remontel, a ship of little significance that has found itself in the middle of an interstellar plot.

The bridge crew and senior officers have all been abducted. It's now up to you to discover what has happened and bring your captain home.

## A Work in Progress

## Disclaimer

Star Trek and all related marks, logos and characters are solely owned by CBS Studios Inc. This fan production is not endorsed by, sponsored by, nor affiliated with CBS, Paramount Pictures, or any other Star Trek franchise, and is a non-commercial fan-made film intended for recreational use. No commercial exhibition or distribution is permitted. No alleged independent rights will be asserted against CBS or Paramount Pictures.
